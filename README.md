# max specific growth rate calculator
The script uses an excel file as input with LN(biomass) values grouped in replicates, to calculate the mumax of the cell culture.

It perfroms a linear regression to determine the slope of the curve that corresponds to the maximum specific growth rate.

LN(biomass) values for the same sample are placed in the same column and each row corresponds to a 1 time point. 
The suggested scheme for the escel files is as follows:

Time points A - Replicate A1- Replicate A2- Replicate A3 - Time points B - Replicate B1- Replicate B2- Replicate B3

The script calculates the mumax for three growth curves (triplicates) at a time. So you will need to run it for as many samples you have m,odifying the location of the columns you wanna calculate the mumax of.