import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import os

import pandas as pd
import seaborn as sns

from sklearn.linear_model import LinearRegression

 # path and name of the file
file_name = "growthdata_Ivan.xlsx"
sheet =  "Sheet2"

data=[]
t =[]
x =[]
y =[]
z =[]

t_all =[]
x_all =[]
y_all =[]
z_all =[]

 # read the file
import pandas as pd
df = pd.read_excel(io=file_name, sheet_name=sheet)
data = np.array(df)
print(data)


print('Reading done')

##########################################################
column_time = 4 #add 4 to change sample
max_row=50
##########################################################
# Let us read the time and the three following columns

t=data[30:max_row,column_time]
x=data[30:max_row,column_time+1]
y=data[30:max_row,column_time+2]
z=data[30:max_row,column_time+3]

t_all=data[0:,column_time]
x_all=data[0:,column_time+1]
y_all=data[0:,column_time+2]
z_all=data[0:,column_time+3]

num = len(t)

##########################################################
#Linear regresion
##########################################################
t_data = [] 
x_data = []
y_data = []
z_data = []

R2_data = []
R2_data2 = []
R2_data3 = []

m = []
n= []
m2 = []
n2= []
m3 = []
n3= []



i=0
for a in range(num):
	#print('a='+str(a))
	for b in range(num):
		#print(b)
		#print('i=' + str(i))
		if (a<(num-b)):
			if (a-(num-b)<-5):
				t_data.append(t[0+a:num-b])
				x_data.append(x[0+a:num-b])
				y_data.append(y[0+a:num-b])
				z_data.append(z[0+a:num-b])


				#print(t_data[i])
				regresion_lineal = LinearRegression()
				regresion_lineal.fit(t_data[i].reshape(-1,1), x_data[i])
				m.append(regresion_lineal.coef_)
				n.append(regresion_lineal.intercept_)
				#print('m = ' + str(regresion_lineal.coef_) + ', n = ' + str(regresion_lineal.intercept_))

				regresion_lineal2 = LinearRegression()
				regresion_lineal2.fit(t_data[i].reshape(-1,1), y_data[i])
				m2.append(regresion_lineal2.coef_)
				n2.append(regresion_lineal2.intercept_)

				regresion_lineal3 = LinearRegression()
				regresion_lineal3.fit(t_data[i].reshape(-1,1), z_data[i])
				m3.append(regresion_lineal3.coef_)
				n3.append(regresion_lineal3.intercept_)

				#R2
				r2 = regresion_lineal.score(t_data[i].reshape(-1,1), x_data[i])
				#print('Coeficiente de Determinación R2 = ' + str(r2))
				R2_data.append( regresion_lineal.score(t_data[i].reshape(-1,1), x_data[i]) )
				R2_data2.append( regresion_lineal2.score(t_data[i].reshape(-1,1), y_data[i]) )
				R2_data3.append( regresion_lineal3.score(t_data[i].reshape(-1,1), z_data[i]) )

				i=i+1
			else:
				break
		else:
			break

#print(R2_data)
maxR2=np.amax(R2_data)
index=R2_data.index(maxR2)
#print('index = ' + str(index))

maxR22=np.amax(R2_data2)
index2=R2_data2.index(maxR22)

maxR23=np.amax(R2_data3)
index3=R2_data3.index(maxR23)

##########################################################
#Show values
##########################################################
print('Value of R^2: ' + str(maxR2))
print('y=' + str(m[index])+'x + '+ str(n[index]))
print('Values of R^2 2:' + str(maxR22))
print('y=' + str(m2[index2])+'x + '+ str(n2[index2]))
print('Values of R^2 3:' + str(maxR23))
print('y=' + str(m3[index3])+'x + '+ str(n3[index3]))


##########################################################
#Plot
##########################################################
t_lr=np.linspace(0,15)
x_lr=np.linspace(0,15)
y_lr= m[index]*x_lr + n[index]
y_lr2= m2[index2]*x_lr + n2[index2]
y_lr3= m3[index3]*x_lr + n3[index3]

fig = plt.figure()

plt.plot(t_all,x_all,'b.')
plt.plot(t_data[index],x_data[index],'.')

plt.plot(t_all,y_all,'r.')
plt.plot(t_data[index2],y_data[index2],'.')

plt.plot(t_all,z_all,'g.')
plt.plot(t_data[index3],z_data[index3],'.')

plt.plot(t_lr,y_lr, 'b')
plt.plot(t_lr,y_lr2, 'r')
plt.plot(t_lr,y_lr3, 'g')

#Change time axes range
##########################################################
plt.xlim([0,20])
plt.ylim([3,6])
##########################################################

plt.show()



